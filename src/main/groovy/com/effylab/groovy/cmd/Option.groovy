package com.effylab.groovy.cmd;

import java.util.regex.Pattern;

import groovy.transform.PackageScope;
import groovy.transform.CompileStatic;

@PackageScope
@CompileStatic
class Option {

    String shortName;
    String longName;
    String byDefault;
    String about;
    boolean isFlag;
    boolean isRequired;
    Pattern namePattern;

    Option(Opt opt) {
        for (String nameToken in opt.name().split('\\|')) {
            if (nameToken.length() == 1) {
                shortName = nameToken;
            } else if (nameToken.length() > 1) {
                longName = nameToken;
            }
        }
        this.byDefault = opt.byDefault();
        this.about = opt.help();
        this.isFlag = opt.flag();
        this.isRequired = opt.required();


        if (isFlag) {
            if (!shortName) {
                namePattern = ~/^--${longName}$/;
            } else if (!longName) {
                namePattern = ~/^-{shortName}$/;
            } else {
                namePattern = ~/^(-${shortName}|--${longName})$/;
            }
        } else {
            if (!shortName) {
                namePattern = ~/^--${longName}=?$/;
            } else if (!longName) {
                namePattern = ~/^-${shortName}=?$/;
            } else {
                namePattern = ~/^(-${shortName}=?|--${longName}=?)$/;
            }
        }
    }

    @Override
    String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(isRequired ? '<' : '[');
        if (shortName) {
            sb.append(shortName);
            if (longName) {
                sb.append('|');
                sb.append(longName);
            }
        } else {
            sb.append(longName);
        }
        sb.append(isRequired ? '>' : ']');
        sb.append('\t- ');
        sb.append(about);
        return sb.toString();
    }
}