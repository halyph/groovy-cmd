package com.effylab.groovy.cmd;

import groovy.transform.PackageScope;
import groovy.transform.CompileStatic;

import java.lang.reflect.Method;

import com.effylab.groovy.cmd.exception.IllegalMethodSignatureException;
import com.effylab.groovy.cmd.exception.MethodReturnTypeException;
import com.effylab.groovy.cmd.exception.MethodArgumentsNumberException;
import com.effylab.groovy.cmd.exception.MethodArgumentTypeException;

@PackageScope
@CompileStatic
class CliLifeCycle {

    Closure printWelcome;
    Closure cliInput;
    Closure printGoodbye;
    Closure printNoCommand;

    private CommandManager commandManager;
    private CommandHistory history;

    CliLifeCycle(Object methodObject, List<Method> methods)
    throws IllegalMethodSignatureException {
        commandManager = new CommandManager(methodObject);
        methods.each {
            commandManager.addCommand((Method) it);
        }
        history = new CommandHistory();
    }

    void runLifeCycle() {
        printWelcome();
        boolean isContinueInput = true;
        Command historyCommand = null;
        while (isContinueInput) {
            String commandsLine = cliInput(historyCommand);
            if (historyCommand) {
                if (commandsLine.equals('q')) {
                    historyCommand = null;
                } else if (!commandsLine) {
                    executeCommand(historyCommand);
                    historyCommand = null;
                } else {
                    historyCommand = historyCommand.replaceArguments(commandsLine);
                }
                continue;
            }
            if (!commandsLine) {
                continue;
            }
            if (commandsLine =~ /!(\d+|!)/) {
                historyCommand = getHistoryCommand(commandsLine);
                continue;
            }
            for (String commandLine : commandsLine.split('\\+')*.trim()) {
                Command command = commandManager.getCommand(commandLine);
                if (!executeCommand(command)) {
                    break;
                }
                if (command.name.equals('exit')) {
                    isContinueInput = false;
                    break;
                }
            }
        }
        printGoodbye();
    }

    boolean executeSingleCommandsLine(String[] args) {
        int lastCommandIndex = 0;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals('+')) {
                String[] arguments = args[lastCommandIndex..(i - 1)] as String[];
                Command command = commandManager.getCommand(arguments);
                if (!executeCommand(command)) {
                    return false;
                }
                lastCommandIndex = i + 1;
            } else if (i == (args.length - 1)) {
                String[] arguments = args[lastCommandIndex..i] as String[];
                Command command = commandManager.getCommand(arguments);
                return executeCommand(command)
            }
        }
        return true;
    }

    List<Command> getCommandsList() {
        return commandManager.getCommandsList();
    }

    Command getCommand(String name) {
        return commandManager.getCommand(name);
    }

    List<Command> getHistoryElements() {
        return history.getElements();
    }

    private boolean executeCommand(Command command) {
        if (!command) {
            printNoCommand();
            return false
        } else if (!command.name.equals('history')) {
            history.addCommand(command);
        }
        return command.invoke();
    }

    private Command getHistoryCommand(String commandName) {
        if (commandName.equals('!!')) {
            return history.getLastCommand();
        }
        int index = (commandName.substring(1).toInteger() - 1);
        if ((index > history.getSize()) || (index < 0)) {
            println("ERROR:\tIndex ${index + 1} is out of history bound.\n\tCurrent history size is - ${history.getSize()}");
            return null;
        }
        return history.getCommand(index);
    }
}