package com.effylab.groovy.cmd;

import groovy.transform.CompileStatic;

import java.lang.reflect.Method;

/**
 * Main CMD framework abstract class. For creation your own cli applications you need to extend it.
 * 
 * Cmd class contains default default list of cli commands:
 * 
 * - help
 * - exit
 * - history
 */
@CompileStatic
abstract class Cmd {

    /**
     * Map contains printable variables with predifined values:
     * 
     * prompt       - cli prompt, default is 'cmd:> '
     * welcome      - welcome output, default is 'Welcome!\nType "help" for command list'
     * goodbye      - goodbye output, default is 'Goodbye!'
     * help         - help output, default is 'List of all available commands:\n(type "help <command_name>" for more info)'
     * noCommand    - no comand output, default is 'There is no such command'
     */
    final Map<String, String> out = [
        prompt:     'cmd:> ',
        welcome:    'Welcome!\nType "help" for command list',
        goodbye:    'Goodbye!',
        help:       'List of all available commands:\n(type "help <command_name>" for more info)',
        noCommand:  'There is no such command'
    ];

    private CliLifeCycle executor;

    /**
     * Constructor
     */
    protected Cmd() {
        Object methodObject = this;
        List<Method> methods = new ArrayList<>(this.getClass().methods.findAll { 
            return ((Method) it).name.startsWith('do_');
        });
        executor = new CliLifeCycle(this, methods);
        executor.printWelcome = {
            println(out.welcome);
        };
        executor.cliInput = { historyCommand ->
            print(out.prompt);
            if (historyCommand) {
                print("${historyCommand} | ");
            }
            return System.console().readLine().trim();
        }
        executor.printGoodbye = {
            println(out.goodbye);
        }
        executor.printNoCommand = { commandName ->
            println(out.noCommand);
        }
    }

    /**
     * Runs interactive console input\output mode cycle
     */
    void start() {
        executor.runLifeCycle();
    }

    /**
     * Single executing of commands, without interactive mode
     * 
     * @params args     list of commands and arguments
     * 
     * @return          true -  if execution was successful
     *                  false - if execution was failed
     */
    boolean execute(String[] args) {
        return executor.executeSingleCommandsLine(args);
    }

    /**
     * Default exit command
     * 
     * @params args     list of commands and arguments
     * 
     * @return          true -  if execution was successful
     *                  false - if execution was failed
     */
    @Description(
        brief='for exit from CMD',
        full='exits from CMD. It doesn\' have any arguments'
    )
    boolean do_exit(String[] args) {
        if (args.length == 0) {
            return true;
        } else {
            println('ERROR:\tCommand "exit" mustn\'t have any arguments');
            return false;
        }
    }

    /**
     * Default help command
     * 
     * @params args     list of commands and arguments
     * 
     * @return          true -  if execution was successful
     *                  false - if execution was failed
     */
    @Description(
        brief='prints commands info',
        full='prints commands info'
    )
    boolean do_help(String[] args) {
        if (args.length > 1) {
            println('ERROR:\tCommand "help" must have only 0 or 1 argument');
            return false;
        }
        if (args.length == 0) {
            println(out.help);
            executor.getCommandsList().each {
                Command command = (Command) it;
                println("\t${command.name}\t - ${command.briefDescription}");
            }
        } else if (args.length == 1) {
            String commandName = args[0];
            Command command = executor.getCommand(commandName);
            if (!command) {
                println("ERROR:\tThere is no such command - '${commandName}'");
                return false;
            }
            println("COMMAND '${commandName}' INFO:\n");
            println(command.fullDescription);
            println();
            if (command.options) {
                List<String> mandatory = new ArrayList<>();
                List<String> optional = new ArrayList<>();
                for (Option option in command.options) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(option.toString());
                    if (option.byDefault) {
                        sb.append('\n');
                        sb.append(' ' * 18);
                        sb.append('Default value is: ');
                        sb.append(option.byDefault);
                    }
                    if (option.isRequired) {
                        mandatory.add(sb.toString());
                    } else {
                        optional.add(sb.toString());
                    }
                }
                println('OPTIONS:\n');
                if (mandatory) {
                    mandatory.each {
                        println(" ${it}");
                    }
                }
                if (optional) {
                    optional.each {
                        println(" ${it}");
                    }
                }
            }
        }
        return true;
    }

    /**
     * Default history command
     * 
     * @params args     list of commands and arguments
     * 
     * @return          true -  if execution was successful
     *                  false - if execution was failed
     */
    @Description(
        brief='prints history of commands',
        full='prints history of commands'
    )
    boolean do_history(String[] args) {
        if (args.length > 1) {
            println('ERROR:\tCommand "history" must have only 0 or 1 argument');
            return false;
        }
        List<Command> commands = executor.getHistoryElements();
        if (args.length == 0) {
            commands.eachWithIndex { command, i ->
                println("${String.format('%1$3s', (((Integer) i) + 1))} - ${command}");
            }
        } else if (args.length == 1) {
            commands.eachWithIndex { command, i ->
                if (((Command) command).name.equals(args[0])) {
                    println("${String.format('%1$3s', (((Integer) i) + 1))} - ${command}");
                }
            }
        }
        return true;
    }
}