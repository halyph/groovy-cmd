package com.effylab.groovy.cmd;

import groovy.transform.PackageScope;
import groovy.transform.CompileStatic;

/**
 * Immutable implementation of 'Map' interface for storing options values.
 * 
 * You can describe your command's options by array of {@link com.effylab.groovy.cmd.Opt} annotations 
 * in {@link com.effylab.groovy.cmd.Description} annotation for your command method.
 *
 * If option doesn't have setted value, a default value returns.
 * 
 * @see     com.effylab.groovy.cmd.Opt
 * 
 * @author  Artem Labazin
 * @version 1.2.2 06 Mar 2014
 */
@CompileStatic
class Options implements Map<String, String> {

    @PackageScope
    Properties options;

    Options(Properties options) {
        this.options = options;
    }
    
    @Override
    void clear() {
        throw new UnsupportedOperationException('Instance of class \'Options\' is immutable');
    }

    @Override
    boolean containsKey(Object key) {
        return options.containsKey(key);
    }

    @Override
    boolean containsValue(Object value) {
        return options.containsValue(value);
    }

    @Override
    Set<Map.Entry<String, String>> entrySet() {
        return options.entrySet();
    }

    @Override
    boolean equals(Object obj) {
        if (!(obj instanceof Options)) {
            return false;
        }
        return options.equals(((Options) obj).options);
    }

    @Override
    String get(Object key) {
        return options.getProperty((String) key);
    }

    @Override
    int hashCode() {
        return options.hashCode();
    }

    @Override
    boolean isEmpty() {
        return options.isEmpty();
    }

    @Override
    Set<String> keySet() {
        return options.keySet();
    }

    @Override
    String put(String key, String value) {
        throw new UnsupportedOperationException('Instance of class \'Options\' is immutable');
    }

    @Override
    void putAll(Map<String, String> m) {
        throw new UnsupportedOperationException('Instance of class \'Options\' is immutable');
    }

    @Override
    String remove(Object key) {
        throw new UnsupportedOperationException('Instance of class \'Options\' is immutable');
    }

    @Override
    int size() {
        return options.size();
    }

    @Override
    Collection<String> values() {
        return options.values();
    }
}