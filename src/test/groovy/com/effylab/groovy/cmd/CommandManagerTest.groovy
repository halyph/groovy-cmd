package com.effylab.groovy.cmd;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import com.effylab.groovy.cmd.exception.IllegalMethodSignatureException;
import com.effylab.groovy.cmd.exception.MethodReturnTypeException;
import com.effylab.groovy.cmd.exception.MethodArgumentsNumberException;
import com.effylab.groovy.cmd.exception.MethodArgumentTypeException;

class CommandManagerTest {

    private static CommandManager manager;

    @Test
    void addCommandTest() {
        InvalidReturnTypeTestClass invalidReturnTypeTestClass = new InvalidReturnTypeTestClass();
        CommandManager manager_1 = new CommandManager(invalidReturnTypeTestClass);
        Method method_1 = invalidReturnTypeTestClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        IllegalMethodSignatureException throwed_1 = null;
        try {
            manager_1.addCommand(method_1);
        } catch (IllegalMethodSignatureException ex) {
            throwed_1 = ex;
        }
        assertNotNull(throwed_1);
        assertEquals(MethodReturnTypeException.class, throwed_1.getClass());

        InvalidArgumentsNumberTestClass invalidArgumentsNumberTestClass = new InvalidArgumentsNumberTestClass();
        CommandManager manager_2 = new CommandManager(invalidArgumentsNumberTestClass);
        Method method_2 = invalidArgumentsNumberTestClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        IllegalMethodSignatureException throwed_2 = null;
        try {
            manager_2.addCommand(method_2);
        } catch (IllegalMethodSignatureException ex) {
            throwed_2 = ex;
        }
        assertNotNull(throwed_2);
        assertEquals(MethodArgumentsNumberException.class, throwed_2.getClass());

        InvalidArgumentTypeTestClass invalidArgumentTypeTestClass = new InvalidArgumentTypeTestClass();
        CommandManager manager_3 = new CommandManager(invalidArgumentTypeTestClass);
        Method method_3 = invalidArgumentTypeTestClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        IllegalMethodSignatureException throwed_3 = null;
        try {
            manager_3.addCommand(method_3);
        } catch (IllegalMethodSignatureException ex) {
            throwed_3 = ex;
        }
        assertNotNull(throwed_3);
        assertEquals(MethodArgumentTypeException.class, throwed_3.getClass());

        ValidTestClass validTestClass = new ValidTestClass();
        CommandManager manager_4 = new CommandManager(validTestClass);
        Method method_4 = validTestClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        IllegalMethodSignatureException throwed_4 = null;
        try {
            manager_4.addCommand(method_4);
        } catch (IllegalMethodSignatureException ex) {
            throwed_4 = ex;
        }
        assertNull(throwed_4);

        ValidTestClass2 validTestClass2 = new ValidTestClass2();
        CommandManager manager_5 = new CommandManager(validTestClass2);
        Method method_5 = validTestClass2.getClass().methods.find {
            it.name.equals('do_test');
        };
        IllegalMethodSignatureException throwed_5 = null;
        try {
            manager_5.addCommand(method_5);
        } catch (IllegalMethodSignatureException ex) {
            throwed_5 = ex;
        }
        assertNull(throwed_5);
    }

    @Test
    void getCommandTest() {
        ValidTestClass validTestClass = new ValidTestClass();
        CommandManager manager = new CommandManager(validTestClass);
        Method method = validTestClass.getClass().methods.find {
            it.name.equals('do_test');
        };
        manager.addCommand(method);

        Command command_1 = manager.getCommand('test');
        assertNotNull(command_1);
        assertEquals('test', command_1.name);
        assertEquals('No description', command_1.briefDescription);
        assertEquals('No description', command_1.fullDescription);
        assertEquals(method, command_1.method);
        assertEquals(new String[0], command_1.arguments);

        Command command_2 = manager.getCommand('test -flag single_value\tkey=value key="long value" key=\'long value\' \'quoted value\' "quoted value"');
        assertNotNull(command_2);
        assertEquals(7, command_2.arguments.length);
        assertEquals('-flag', command_2.arguments[0]);
        assertEquals('single_value', command_2.arguments[1]);
        assertEquals('key=value', command_2.arguments[2]);
        assertEquals('key=long value', command_2.arguments[3]);
        assertEquals('key=long value', command_2.arguments[4]);
        assertEquals('quoted value', command_2.arguments[5]);
        assertEquals('quoted value', command_2.arguments[6]);

        Command command_3 = manager.getCommand('popa');
        assertNull(command_3);
    }

    public static class ValidTestClass {

        boolean do_test(String[] args) {
            return true;
        }
    }

    public static class ValidTestClass2 {
        boolean do_test(Options opts, String[] args) {
            return true;
        }
    }

    public static class InvalidReturnTypeTestClass {

        void do_test(String[] args) {
            return;
        }
    }

    public static class InvalidArgumentsNumberTestClass {

        boolean do_test(String[] args, Integer index, String some) {
            return true;
        }
    }

    public static class InvalidArgumentTypeTestClass {

        boolean do_test(Integer index) {
            return true;
        }
    }
}